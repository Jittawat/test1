import React, { useEffect, useState } from "react";
import { v4 as uuidv4 } from "uuid";

const App = () => {
  const [todos, setTodos] = useState(() => {
    const localValue = JSON.parse(localStorage.getItem("TODOS"));
    return localValue ? localValue : [];
  });
  const [inputVal, setInputVal] = useState("");
  const [isEditing, setIsEditing] = useState(null);

  const handleSubmit = (e) => {
    e.preventDefault();
    handleAdd();
  };

  useEffect(() => {
    localStorage.setItem("TODOS", JSON.stringify(todos));
  }, [todos]);

  const handleAdd = () => {
    if (!inputVal.trim()) return;
    if (isEditing) {
      const newTodo = todos.map((item) =>
        item.id === isEditing ? { ...item, text: inputVal } : item
      );
      setTodos(newTodo);
      setIsEditing(null);
    } else {
      setTodos([...todos, { id: uuidv4(), text: inputVal, completed: false }]);
      setInputVal("");
    }
    setInputVal("");
  };

  const handleDelete = (id) => {
    const newTodo = todos.filter((item) => item.id !== id);
    setTodos(newTodo);
  };

  const handleEdit = (item) => {
    setInputVal(item.text);
    setIsEditing(item.id);
  };

  const handleToggle = (id) => {
    const newTodo = todos.map((item) =>
      item.id === id ? { ...item, completed: !item.completed } : item
    );
    setTodos(newTodo);
  };

  return (
    <div className="bg-slate-400 h-screen">
      <p className="text-7xl text-center text-gray-50">TODO LIST</p>
      <div className="flex justify-center items-center">
        <form onSubmit={handleSubmit} className="bg-slate-300 mt-10 w-1/3">
          <div className="flex items-center gap-4">
            <input
              value={inputVal}
              onChange={(e) => setInputVal(e.target.value)}
              className="py-3 px-5 w-full rounded-md text-black outline-none text-2xl"
              type="text"
              placeholder="Enter your todo..."
            />
            <span
              onClick={handleAdd}
              className="py-3 px-5 cursor-pointer bg-blue-500 font-semibold rounded-md"
            >
              Add
            </span>
          </div>

          <ul className="flex flex-col gap-4 mt-5">
            {todos.length <= 0 && (
              <div className="text-xl font-semibold text-red-500 text-center">
                THERE IS NO TODO HERE....
              </div>
            )}
            {todos.map((item) => (
              <li
                key={item.id}
                className="flex items-center p-3 rounded-md transition-all hover:bg-black hover:bg-opacity-50 justify-between"
              >
                <div className="flex items-center gap-3">
                  <input
                    checked={item.completed}
                    onChange={() => handleToggle(item.id)}
                    className="w-5 h-5"
                    type="checkbox"
                  />
                  <span className={`${item.completed ? "line-through" : ""}`}>
                    {item.text}
                  </span>
                </div>
                <div className="flex items-center gap-3">
                  <span
                    onClick={() => handleEdit(item)}
                    className="w-6 h-6 flex cursor-pointer  items-center justify-center bg-blue-200 rounded-full text-blue-500"
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke-width="1.5"
                      stroke="currentColor"
                      class="w-6 h-6"
                    >
                      <path
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L10.582 16.07a4.5 4.5 0 01-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 011.13-1.897l8.932-8.931zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0115.75 21H5.25A2.25 2.25 0 013 18.75V8.25A2.25 2.25 0 015.25 6H10"
                      />
                    </svg>
                  </span>
                  <span
                    onClick={() => handleDelete(item.id)}
                    className="w-6 h-6 flex  cursor-pointer items-center justify-center bg-red-200 rounded-full text-red-500"
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke-width="1.5"
                      stroke="currentColor"
                      class="w-6 h-6"
                    >
                      <path
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0"
                      />
                    </svg>
                  </span>
                </div>
              </li>
            ))}
          </ul>
        </form>
      </div>
    </div>
  );
};

export default App;
